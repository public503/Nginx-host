#
# ubuntu 18.04
#

#
# pre install
apt-get update -y
apt-get install -y nload htop screen nano git wget nginx

add-apt-repository universe
apt-get install -y php-fpm php-common php-mysql php-gd php-curl php-mbstring 

# 
# config
cd /etc/nginx
rm -rf sites-enabled
ln -s sites-available sites-enabled

